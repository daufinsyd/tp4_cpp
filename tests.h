//
// Created by sygems on 10/5/16.
//

#ifndef TP4_TESTS_H
#define TP4_TESTS_H


#include <fstream>
#include <iostream>
#include <gtest/gtest.h>
#include "Rectangle.h"
#include "Point.h"
#include "Forme.h"

TEST(Point, Instanciation) {
Point p1;
ASSERT_EQ(p1.getX(), 0);
ASSERT_EQ(p1.getY(), 0);

p1.setX(11);
p1.setY(21);

ASSERT_EQ(p1.getX(), 11);
ASSERT_EQ(p1.getY(), 21);

Point p2(12, 22);

ASSERT_EQ(p2.getX(), 12);
ASSERT_EQ(p2.getY(), 22);
}

TEST(Point, Origine) {
	ASSERT_EQ(Point::ORIGINE.getX(), 0);
	ASSERT_EQ(Point::ORIGINE.getY(), 0);
}


TEST(Forme, Compteur) {
   // Pour ?tre correct, ce test doit etre le premier sur Forme
   ASSERT_EQ(0, Forme::prochainId());
   Forme f1;
   ASSERT_EQ(0, f1.getId());
   ASSERT_EQ(1, Forme::prochainId());
   // Verification que la valeur n'est pas decrementee accidentellement.
   Forme *p = new Forme;
   ASSERT_EQ(1, p->getId());
   delete p;
   ASSERT_EQ(2, Forme::prochainId());
}


TEST(Forme, Instanciation1) {
	Forme f1;
	ASSERT_EQ(f1.getPosition().getX(), 0);
	ASSERT_EQ(f1.getPosition().getY(), 0);
	ASSERT_EQ(f1.getCouleur(), BLEU);
}


TEST(Forme, Instanciation2) {
	Forme f2;

	f2.setX(15);
	f2.setY(25);
	f2.setCouleur(VERT);
	ASSERT_EQ (f2.getPosition().getX(), 15);
	ASSERT_EQ (f2.getPosition().getY(), 25);
	ASSERT_EQ (f2.getCouleur(), VERT);
	ASSERT_NE (f2.getCouleur(), BLEU);
	ASSERT_NE (f2.getCouleur(), ROUGE);
	ASSERT_NE (f2.getCouleur(), JAUNE);
}


TEST(Forme, Instanciation3) {
    // IL N'Y A PAS D'ERREUR DANS LE TEST, CELA DOIT MARCHER
	Forme f2(Point(10,20), ROUGE);
	ASSERT_EQ (f2.getPosition().getX(), 10);
	ASSERT_EQ (f2.getPosition().getY(), 20);
	ASSERT_EQ (f2.getCouleur(), ROUGE);
	ASSERT_NE (f2.getCouleur(), BLEU);

	f2.setX(15);  //f2.getPosition().setX(15);
	f2.setY(25);
	f2.setCouleur(JAUNE);
	ASSERT_EQ (f2.getPosition().getX(), 15);
	ASSERT_EQ (f2.getPosition().getY(), 25);
	ASSERT_EQ (f2.getCouleur(), JAUNE);
	ASSERT_NE (f2.getCouleur(), BLEU);
	ASSERT_NE (f2.getCouleur(), ROUGE);
}


TEST(Forme, BoiteEnglobante) {
	Forme f;
	ASSERT_EQ (f.getLargeur(), 1);
	ASSERT_EQ (f.getHauteur(), 1);
}



#endif //TP4_TESTS_H
