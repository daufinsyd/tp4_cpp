//
// Created by sygems on 10/5/16.
//

#ifndef TP4_POINT_H
#define TP4_POINT_H


class Point {
public:

    Point(int = 0, int = 0);

    void afficher() const;
    int getX() const;
    int getY() const;

    void setY(int);
    void setX(int);

    static const Point ORIGINE;

private:

    int _x;
    int _y;


};

extern Point ORIGINE;

#endif //TP4_POINT_H
