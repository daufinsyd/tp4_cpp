//
// Created by sygems on 10/5/16.
//

#include <iostream>
#include "Forme.h"


int Forme::nextId_ = 0;


Forme::Forme() {

    id_ = nextId_;

    largeur_ = 1;
    hauteur_ = 1;
    couleur_ = BLEU;
    position_ = Point(0,0);

    std::cout << "Forme cr?e" << std::endl;
    nextId_++;
}

Forme::Forme(Point p, COULEURS c) {

    id_ = nextId_;

    largeur_ = 1;
    hauteur_ = 1;
    couleur_ = c;
    position_ = p;

    std::cout << "Forme cr?e" << std::endl;
    nextId_++;
}

void Forme::afficher() const{
    std::cout << "Point: l,h,c" << largeur_ << ' ' << hauteur_ << ' ' << couleur_;
    std::cout << " Position: x,y " << position_.getX() << ' ' << position_.getY() << std::endl;
}

void Forme::deplacer(Point& destP) {
    position_ = destP;
}

bool Forme::contient(const Point& p) const {
    bool code = false;

    int x = position_.getX();
    int y = position_.getY();

    if(p.getX() > x && p.getX() < x + largeur_ && p.getY() < y && p.getY() < y + hauteur_) {
        code = true;
    }

    return code;
}


int Forme::getLargeur() const{
    return largeur_;
}

int Forme::getHauteur() const {
    return hauteur_;
}

Point Forme::getPosition() const {
    return position_;
}

std::string Forme::toString() {
    return "";
}

int Forme::prochainId(){
    return nextId_;
}

int Forme::getId() const {
    return id_;}

COULEURS Forme::getCouleur() const {
    return couleur_;}

void Forme::setX(int x) {
    position_.setX(x);
}

void Forme::setY(int y) {
    position_.setY(y);
}

void Forme::setCouleur(COULEURS c) {
    couleur_ = c;
}