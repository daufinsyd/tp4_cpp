cmake_minimum_required(VERSION 3.3)
project(TP4)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp Point.cpp Point.h Forme.cpp Forme.h Cercle.cpp Cercle.h Rectangle.cpp Rectangle.h tests.cpp tests.h)
add_executable(TP4 ${SOURCE_FILES})