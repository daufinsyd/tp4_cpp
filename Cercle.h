//
// Created by sygems on 10/5/16.
//

#ifndef TP4_CERCLE_H
#define TP4_CERCLE_H


#include "Forme.h"

class Cercle : public Forme {
public:
    Cercle(Point, int = 1, int = 1);
    Cercle(Point, int = 1);

};


#endif //TP4_CERCLE_H
