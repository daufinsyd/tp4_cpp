//
// Created by sygems on 10/5/16.
//

#include <iostream>
#include "Point.h"

const Point Point::ORIGINE = Point(0,0);


Point::Point(int x, int y) {
    _x = x;
    _y = y;
}

void Point::afficher() const {
    std::cout << _x << " " << _y << std::endl;
}

int Point::getX() const{
    return _x;
}

int Point::getY() const {
    return _y;
}

void Point::setX(int x) {
    _x = x;
}

void Point::setY(int y){
    _y = y;
}