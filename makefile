# Exemple de makefile un peu automatique

# compilateur
CC = g++
# options de compilation
CCFLAGS =-Wall -Wextra -g
# options de l'edition des liens
LDFLAGS =-lm
# liste des fichiers objets
OBJ =main.o Forme.o Cercle.o Rectangle.o Point.o
OBJ_test =main.o Forme.o Cercle.o Rectangle.o Point.o
EXE =executable

# regle finale pour la creation de l'executable
executable:$(OBJ)
	$(CC) $(LDFLAGS) -o $@ $(OBJ)

# compiler tous les .cpp en .o
.cpp.o:
	$(CC) $(CCFLAGS) -c $<

# creation automatique des dependances
# attention : on liste tous les fichiers cpp du repertoire mais on peut affiner ;-)
dep:
	$(CC) -MM *.cpp > makefile.dep

clean:
	rm -rf $(OBJ) core *.h.gch

test: $(OBJ)
	$(CC) -o test tests.cpp Forme.cpp Point.cpp /opt/gtest/lib/libgtest.a -pthread -I/opt/gtest/include

include makefile.dep
