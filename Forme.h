//
// Created by sygems on 10/5/16.
//

#ifndef TP4_FORME_H
#define TP4_FORME_H


#include "Point.h"
#include <iostream>

enum COULEURS { BLANC, NOIR, VERT, BLEU, ROUGE, JAUNE};

class Forme {
public:
    Forme();
    Forme(Point, COULEURS);
    std::string toString();
    virtual void afficher() const;
    void deplacer(Point&);
    bool contient(const Point&) const;

    int getLargeur() const;
    int getHauteur() const;
    Point getPosition() const;

    COULEURS getCouleur() const;

    static int prochainId();
    int getId() const;

    void setX(int);
    void setY(int);
    void setCouleur(COULEURS);


private:
    int id_;  // de l'instance
    static int nextId_;  // de la classe (stocke le nb d'instances cr?es)

    Point position_;
    int largeur_;
    int hauteur_;
    COULEURS couleur_;



};



#endif //TP4_FORME_H
